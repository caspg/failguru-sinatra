require 'net/http'
require 'rubygems'
require 'sinatra'

get '/' do
  "Hi There, I'm failguru sinatra client!"
end

get '/circle_status' do
  status = params[:status]
  user = params[:user]
  command = status.to_s == 1.to_s ? 'turn_on' : 'turn_off'

  uri = URI("http://localhost:4321/api/robots/arduino/commands/#{command}")
  Net::HTTP.get(uri)
end
