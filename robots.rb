require 'artoo'

class Circle < Artoo::Robot
  connection :firmata, adaptor: :firmata, port: '/dev/ttyUSB1'
  device :board, driver: :device_info
  device :led, driver: :led, pin: 8
  api host: '127.0.0.1', port: '4321'

  work do
    puts "Dupa start"
    led.off
  end

  def turn_on
    puts "DUpa"
    led.on
  end

  def turn_off
    puts "Off"
    led.off
  end
end

robots = []
robots << Circle.new(name: "arduino", commands: [:turn_on, :turn_off])
Circle.work!(robots)
